#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN //nama variabel NRF dan pin konfigurasi

const byte address[6] = "00001"; //Alamat NRF

byte buttonState = 0; // Kondisi pushbotton

void setup() {
  radio.begin(); // permulaan NRF "WAJIB"
  radio.openWritingPipe(address); // Membuka alamat NRF
  radio.setPALevel(RF24_PA_MIN); // jarak NRF bisa Min bisa MAX
  radio.stopListening(); // Sebagai TRansmitter
  Serial.begin(9600); // baudrate serial

  pinMode(5,INPUT); // input pushbotton
}
void loop() {
  buttonState = digitalRead(5); // pushbutton
  if(buttonState == LOW){
    char text[] = "1"; 
    radio.write(&text, sizeof(text)); // Mengirimkan char '1' 
    Serial.println(buttonState); //serial print
  }
  else
  {
    char text[] = "0"; 
    radio.write(&text, sizeof(text)); // Mengirimkan char '1' 
    Serial.println(buttonState); //Serial Print
  }
}
