#include <EEPROM.h>
#define EEPROM_SIZE 200
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
RF24 radio(9,10); // CE, CSN
RF24Network network(radio);      // Include the radio in the network
const uint16_t this_node = 00;   // Address of this node in Octal format ( 04,031, etc)
const uint16_t node01 = 01;      

//const byte address[6] = "00001";

//---Global Variabel---
byte   size_Data  = 10;
const byte  banyak_Pin = 10;
unsigned long waktu,
              previousMillis = 0,
              intervalkirim=60000;
long int timer,timer2;
char rst=0;
int     data_Pin[banyak_Pin]={34,35,32,33,25,26,27,14,12,13},
        data_button[banyak_Pin],
        last[banyak_Pin]={1,1,1,1,1,1,1,1,1,1},
        current_data[banyak_Pin],
        T_eeprom[banyak_Pin],
        C_eeprom[banyak_Pin],
        T_address[banyak_Pin]={0,1,2,3,4,5,6,7,8,9},
        C_address[banyak_Pin]={11,12,13,14,15,16,17,18,19,20},
        flag=0,
        counter_currentState=0;

String data_Value[banyak_Pin], 
       last_State[banyak_Pin],
       last_data[banyak_Pin]="1",
       data_Name[banyak_Pin] = {"1","2","3","4","5","6","7","8","9","10"},
       project_Name= "smartdevice",
       device_Name= "oee";

struct counting{
  long int timer,ct;
  int count,cc;
}; 
counting pushbutton[10];

void setup() {
  Serial.begin(115200);
  for(byte i=0; i< banyak_Pin;i++)pinMode(data_Pin[i],INPUT);
  EEPROM.begin(EEPROM_SIZE);
//  reset_eeprom();
  baca_eeprom();
  SPI.begin();
  radio.begin();
  network.begin(90,this_node);
//  radio.openWritingPipe(address);
//  radio.setPALevel(RF24_PA_MIN);
//  radio.stopListening();
}
void kirim_nrf(){
  const char text[]="kirim data";
  radio.write(&text, sizeof(text));
}
void loop() {
  // put your main code here, to run repeatedly:
  readButton();
  processButton();
  debug();
  if (Serial.available() > 0) {    // is a character available?
    rst = Serial.read();       // get the character
      if ((rst >= '0') && (rst <= '9')) {
      Serial.print("Number received: ");
      Serial.println(rst);
      if(rst=='1'){ //jika input serial monitor 1
        reset_eeprom();   //reset nilai EEPROM jadi 0
        Serial.print("reset nilai EEPROM!");
        baca_eeprom();
      }
    }
  }
}

void processButton(){
   int d=0;
   for(int c=0;c<banyak_Pin;c++){
    if(data_button[c]==0){          //Jika Push Button Ditekan
      pushbutton[c].timer++;        // Start Timer
      pushbutton[c].ct++;     
    }   
        EEPROM.write(T_address[c],pushbutton[c].ct);  //tulis pushbutton timer di eeprom
        EEPROM.commit();
    if(data_button[c]==1) {   
   // Jika Push Button tidak ditekan
      if(pushbutton[c].timer > 0){     // Jika nilai Timer lebih dari 0
        pushbutton[c].count++;
        pushbutton[c].cc++;
        EEPROM.write(T_address[c],pushbutton[c].ct);  //tulis pushbutton timer di eeprom
        EEPROM.commit();
      }
      pushbutton[c].timer=0;
      EEPROM.write(C_address[c],pushbutton[c].cc);  //tulis pushbutton timer di eeprom
      EEPROM.commit();
   }   
  }
  delay(1000);
}

void reset_eeprom(){
  for(int rct=0;rct<banyak_Pin;rct++){
    EEPROM.write(T_address[rct],0);
    EEPROM.commit();
  }
  for(int rcc=0;rcc<banyak_Pin;rcc++){
    EEPROM.write(C_address[rcc],0);
    EEPROM.commit();
  }
}

void baca_eeprom(){  
  for(int y=0;y<banyak_Pin;y++){
    T_eeprom[y]=EEPROM.read(T_address[y]);
    pushbutton[y].ct=T_eeprom[y]; 
  }
  for(int yc=0;yc<banyak_Pin;yc++){ 
    C_eeprom[yc]=EEPROM.read(C_address[yc]);
    pushbutton[yc].cc=C_eeprom[yc]; 
  }  
}

void readButton(){
   for(int p=0;p<banyak_Pin;p++)data_button[p]=digitalRead(data_Pin[p]);
}

void debug(){
  
  Serial.print("T: ");
  for(int pt=0;pt<banyak_Pin;pt++){
    Serial.print(pushbutton[pt].timer);
    Serial.print(" ");
  }
    
  Serial.print(" C: ");
  for(int pc=0;pc<banyak_Pin;pc++){
    Serial.print(pushbutton[pc].count);
    Serial.print(" ");
  }
  Serial.println();
  baca_eeprom();
  Serial.print(" T_eeprom: ");

  for(int yy=0;yy<banyak_Pin;yy++){
    Serial.print(T_eeprom[yy]);
    Serial.print(" ");
  }
  Serial.print(" C_eeprom: ");
  for(int yz=0;yz<banyak_Pin;yz++){
    Serial.print(C_eeprom[yz]);
    Serial.print(" ");
  }
  Serial.println();
}
