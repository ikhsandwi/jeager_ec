#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>



RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";
int LED1 = 2;
int LED2 = 3;
String in = "";
void setup() {
  Serial.begin(9600);
  radio.begin();//Mulai masuk ke NRF
  radio.openReadingPipe(0, address);//Mulai reading untuk alamat NRF
  radio.setPALevel(RF24_PA_MIN);//Jarak antar NRF (bisa minimum bisa maximum)
  radio.startListening();//Mulai "MENDENGARKAN" dengan kata lain sebagai Receiver
  pinMode(LED1, OUTPUT);//LED 1 KAMI IBARATKAN SEBAGAI PUSHBUTTON 1
  digitalWrite(LED1, LOW);
  pinMode(LED2, OUTPUT);//LED 2 KAMI IBARATKAN SEBAGAI PUSHBUTTON 2
  digitalWrite(LED2, LOW);
}
void loop() {
  if (radio.available()) { //UNTUK MENGECEK APAKAH NRF NYA ADA *AVAILABLE* ATAU TIDAK
    char text[2] = "";
    radio.read(&text, sizeof(text)); //NRF MULAI MEMBACA KIRIMAN TRANSMITTER SEBAGAI RECEIVER
    Serial.println(text);
    in = text; //"text" dijadikan String sesuai "in" sebagai variabel INPUT DARI TRANSMITTER 
    if (in == "1") {
      Serial.println("LED 1 MENYALA"); //PUSHBUTTON 1 DITEKAN
      digitalWrite(LED1, HIGH);
    }
    else if(in == "2"){ //PUSHBUTTON 2 DITEKAN
      Serial.println("LED 2 MENYALA");
      digitalWrite(LED2, HIGH);
    }
    else if (in == "00"){ //PUSHBUTTON 2 DILEPAS
      Serial.println("LED2 MATI");
      digitalWrite(LED2, LOW);
    }
    else if(in == "0"){ //PUSHBUTTON 1 DILEPAS
      Serial.println("LED1 MATI");
      digitalWrite(LED1, LOW);
    }
  }
}
