/*
  Titus S
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
RF24 radio(7, 8);                      // CE, CSN
const int sw1 = 5;                     // SW1 di Pin 5 arduino
const byte address[6] = "00001";      // Alamat NRF
int pb = 0;                           // kondisi awal pushButton
char in;                              // tipe data in adalah in

void setup() {
  radio.begin();                    //permulaan NRF
  radio.openWritingPipe(address);   //Membuka Alamat NRF
  radio.setPALevel(RF24_PA_MIN);    // Jarak NRF
  radio.stopListening();            // NRF sebagai Transmitter
  Serial.begin(9600);               //Baudrate SeriaL
  pinMode(sw1, INPUT);              // SW 1 sebagai input
}
void loop() {
  pb = digitalRead(sw1);
  if (pb == LOW)                  // ketika pushbutton ditekan
  {
    char text[] = "2";            // isi data text = 2
    Serial.println("SW1 ON");     // print "SW ON" pada Serial Monitor
    radio.write(&text, (text));   // mengrim data pada Variabel text ke NRF
  }
  else                             // ketika pushbutton tidak ditekan
  {
    Serial.println("SW1 OFF");     //  print "SW OFF" pada Serial Monitor
    char text[] = "00";            // Isi data text = 00
    radio.write(&text, (text));    // mengrim data pada Variabel text ke NRF
  }
}
